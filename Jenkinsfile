pipeline{
    agent any

    environment{
        DB_HOST='mongo-server'
        DB_PUERTO='27017'
        GCP_PROJECT_ID='curso-devops-hany-333400'
        GCP_ARTIFACT_REGISTRY_REGION = 'us-central1-docker.pkg.dev'
        GCP_ARTIFACT_REGISTRY_REPOSITORY = "${env.BRANCH_NAME == "develop" ? "desarrollo" : "testing"}"
        GCP_GCLOUD_PATH = "/opt/google-cloud-sdk/bin"
        GCP_SERVICE_JENKINS= credentials("service-jenkins")
    }

    tools{
        maven 'Maven 3.8.1'
        jdk 'jdk11'
    }

    stages{
        stage('Compilar'){
            steps{
                echo 'Compilando..' //se imprime este texto en consola
                sh 'mvn clean compile' //sh permite ejecutar comandos en jenkins, abre terminal de jenkins sh es jenkins de linux hay otro para Win
            }
        }

        stage('Pruebas'){
            steps{
                echo 'Ejecutando pruebas unitarias e integrales'
                sh 'mvn test -Dspring.profiles.active=test'
            }

            post{
                success{
                    junit "target/surefire-reports/*.xml"
                }
            }
        }

        stage('Analisis de Codigo estático'){
            steps{
                  echo 'Analizando código..'
                  withSonarQubeEnv('sonarqube-server'){ //esto es un entorno q configuro en jenkins con las credenciales de inicio.
                     sh "mvn sonar:sonar -Dsonar.host.url=http://sonarqube:9000 -Dsonar.projectVersion=${BUILD_NUMBER}"
                  }

                  timeout(time:3, unit: 'MINUTES'){
                      //waitForQualityGate abortPipeline: true
                  }
                 
            }
          
        }

        stage('Build'){
            steps{
                echo 'Generando build..'
                sh "mvn package -DskipTests"
               // sh "docker build -t hanycloud/servicio-seguridad:$BUILD_NUMBER ." 
               
               sh "docker build -t $GCP_ARTIFACT_REGISTRY_REGION/$GCP_PROJECT_ID/$GCP_ARTIFACT_REGISTRY_REPOSITORY/servicio-seguridad-taller-final:$BUILD_NUMBER ."
            }
        }

     
        stage('Publicar Build'){
            steps{
                echo 'Publicando imagen en google artifact registry...'
                sh "docker login -u _json_key --password-stdin https://$GCP_ARTIFACT_REGISTRY_REGION < $GCP_SERVICE_JENKINS "
                  sh "docker push $GCP_ARTIFACT_REGISTRY_REGION/$GCP_PROJECT_ID/$GCP_ARTIFACT_REGISTRY_REPOSITORY/servicio-seguridad-taller-final:$BUILD_NUMBER"
            }

            post{
                always{
                    echo 'Eliminando imagenes locales...'
                    sh "docker rmi $GCP_ARTIFACT_REGISTRY_REGION/$GCP_PROJECT_ID/$GCP_ARTIFACT_REGISTRY_REPOSITORY/servicio-seguridad-taller-final:$BUILD_NUMBER"
                }
            }
        }

        stage('Desplegar en Desarrollo'){
            when{
                branch 'develop'

            }
            steps{
                echo 'Desplegando en ambiente desarrollo..'
                sh '$GCP_GCLOUD_PATH/gcloud auth activate-service-account service-jenkins@curso-devops-hany-333400.iam.gserviceaccount.com --key-file=$GCP_SERVICE_JENKINS'//autenticarnos a GCP_GCLOUD_PATH
                sh "$GCP_GCLOUD_PATH/gcloud run deploy servicio-seguridad-taller-final-dev --image=$GCP_ARTIFACT_REGISTRY_REGION/$GCP_PROJECT_ID/$GCP_ARTIFACT_REGISTRY_REPOSITORY/servicio-seguridad-taller-final:$BUILD_NUMBER --region=us-central1 --port=8080 --min-instances=1 --allow-unauthenticated --set-env-vars='SPRING.PROFILES.ACTIVE=dev, DB_HOST=localhost, DB_PUERTO=27017' --project=$GCP_PROJECT_ID" //desplegar imagen

            }
      
          }

    
            stage('Desplegar en Testing'){
            when{
                branch 'testing'

            }
            steps{
                echo 'Desplegando en ambiente testing..'
                sh '$GCP_GCLOUD_PATH/gcloud auth activate-service-account service-jenkins@curso-devops-hany-333400.iam.gserviceaccount.com --key-file=$GCP_SERVICE_JENKINS'//autenticarnos a GCP_GCLOUD_PATH
                sh "$GCP_GCLOUD_PATH/gcloud run deploy servicio-seguridad-taller-final-test --image=$GCP_ARTIFACT_REGISTRY_REGION/$GCP_PROJECT_ID/$GCP_ARTIFACT_REGISTRY_REPOSITORY/servicio-seguridad-taller-final:$BUILD_NUMBER --region=us-central1 --port=8080 --min-instances=1 --allow-unauthenticated --set-env-vars='SPRING.PROFILES.ACTIVE=test, DB_HOST=localhost, DB_PUERTO=27017' --project=$GCP_PROJECT_ID"//desplegar imagen

            }

        }
}
}